CXX := g++
CXXFlags := -Wall -g -pipe -O2 -march=native -D_GLIBCXX_ASSERTIONS -std=c++20

.PHONY: all clean sudoku

all: sudoku

sudoku:
	$(CXX) -o bin/sudoku src/main.cpp src/sudoku.cpp $(CXXFlags)

install: clean
	mkdir -p bin/
	$(MAKE) all


clean:
	rm -rf bin/
