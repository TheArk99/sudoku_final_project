#ifndef __SUDOKU__
#define __SUDOKU__

#include <iostream>
#include <cstdlib>
#include <unistd.h>
#include <pthread.h>
#include <vector>
#include <algorithm>
using namespace std;

const unsigned int ROW = 9;
const unsigned int COLUMN = 9;


class Sudoku {
private:
  struct Cell {
    //define possibleSolutions as all possible until not
    //index 0 is the only true solution (it should remain zero until it is found)
    int possibleSolutions[10] = { 0, 1, 1, 1, 1, 1, 1, 1, 1, 1 };
  };

  Cell board[ROW][COLUMN];

  struct Coordinate {
   int row, column;
  };

  Coordinate CellsInRow[8];
  Coordinate CellsInColumn[8];
  Coordinate CellsInQuadrant[8];

  bool IsEmpty(Coordinate coord);
  int checkHowManyPossibleSolutions(Coordinate coord);
  int getPossibleSolutioin(Coordinate coord);
  void* GetRowCells(void* vcoord);
  void* GetColumnCells(void* vcoord);
  void* GetQuadrantCells(void* vcoord);
  void* SearchRowCells(void* vargs);
  void* SearchColumnCells(void* vargs);
  void* SearchQuadrantCells(void* vargs);
  bool SearchInSolution(int number);
  void* SearchPossibleQuadrantCells(void* vargs);
  void* SearchPossibleRowCells(void* vargs);
  void* SearchPossibleColumnCells(void* vargs);
  bool SearchPossibleInSolution(int number);
//  void* SearchAdjacentRowCells(void* vargs);
//  void* SearchAdjacentColumnCells(void* vargs);
//  bool SearchAdjacent(int number);
  void findPossibleSolutions(Coordinate coord);

public:
  //Sudoku();
  void printBoard();
  void createPuzzle();
  void PrintSolution(Coordinate coord);
  void testing();
  void SolvePuzzle();
  bool isSolved(void);
};

#endif

