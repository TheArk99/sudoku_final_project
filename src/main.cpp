#include <iostream>
#include "sudoku.h"
using namespace std;

int main(void){
  Sudoku s;

  s.createPuzzle();
  s.printBoard();

  while (s.isSolved() != true){
    s.SolvePuzzle();
  }

  s.printBoard();

//  s.testing();
  return 0;
}
