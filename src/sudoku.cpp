#include "sudoku.h"

//makes puzzle based off of dr hwang's 1st puzzle
void Sudoku::createPuzzle(){
  //goes through each solution to populate it with the correct answers for this particular puzzle

  /* // old ones
  board[0][0].possibleSolutions[0] = 8;
  board[0][2].possibleSolutions[0] = 2;
  board[0][3].possibleSolutions[0] = 9;
  board[0][5].possibleSolutions[0] = 3;
  board[2][0].possibleSolutions[0] = 6;
  board[2][2].possibleSolutions[0] = 7;
  board[2][3].possibleSolutions[0] = 8;
  board[3][2].possibleSolutions[0] = 8;
  board[3][4].possibleSolutions[0] = 7;
  board[3][6].possibleSolutions[0] = 1;
  board[3][7].possibleSolutions[0] = 6;
  board[4][8].possibleSolutions[0] = 8;
  board[5][0].possibleSolutions[0] = 9;
  board[5][4].possibleSolutions[0] = 5;
  board[5][6].possibleSolutions[0] = 7;
  board[5][7].possibleSolutions[0] = 4;
  board[6][3].possibleSolutions[0] = 4;
  board[6][5].possibleSolutions[0] = 5;
  board[6][6].possibleSolutions[0] = 6;
  board[6][7].possibleSolutions[0] = 9;
  board[6][8].possibleSolutions[0] = 7;
  board[7][2].possibleSolutions[0] = 6;
  board[7][3].possibleSolutions[0] = 7;
  board[7][4].possibleSolutions[0] = 8;
  board[7][7].possibleSolutions[0] = 2;
  board[8][3].possibleSolutions[0] = 3;
  board[8][4].possibleSolutions[0] = 9;
  board[8][6].possibleSolutions[0] = 4;
  */

  //new board

//  /*
  board[0][2].possibleSolutions[0] = 9;
  board[0][3].possibleSolutions[0] = 4;
  board[0][8].possibleSolutions[0] = 2;
  board[1][0].possibleSolutions[0] = 5;
  board[1][3].possibleSolutions[0] = 8;
  board[1][5].possibleSolutions[0] = 6;
  board[1][8].possibleSolutions[0] = 1;
  board[2][1].possibleSolutions[0] = 6;
  board[2][5].possibleSolutions[0] = 9;
  board[2][7].possibleSolutions[0] = 4;
  board[2][8].possibleSolutions[0] = 5;
  board[4][0].possibleSolutions[0] = 1;
  board[4][2].possibleSolutions[0] = 3;
  board[4][5].possibleSolutions[0] = 5;
  board[4][7].possibleSolutions[0] = 6;
  board[5][0].possibleSolutions[0] = 4;
  board[5][1].possibleSolutions[0] = 5;
  board[5][2].possibleSolutions[0] = 6;
  board[5][3].possibleSolutions[0] = 3;
  board[7][0].possibleSolutions[0] = 8;
  board[7][1].possibleSolutions[0] = 1;
  board[7][2].possibleSolutions[0] = 2;
  board[7][3].possibleSolutions[0] = 6;
  board[7][7].possibleSolutions[0] = 3;
  board[8][0].possibleSolutions[0] = 9;
  board[8][3].possibleSolutions[0] = 5;
  board[8][4].possibleSolutions[0] = 1;
  board[8][5].possibleSolutions[0] = 7;
//  */

}




/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//prints sudoku board
void Sudoku::printBoard(){
  //for printing the grid
  cout << "  ";
  for (int i = 0; i < 9; i++){
    cout << i << " ";
  }
  cout << endl;
  for (int i = 0; i < 9; i++){
    cout << i;
    cout << " ";
    //for printing solutions
    for (int j = 0; j < 9; j++){
      cout << board[i][j].possibleSolutions[0];
      cout << " ";
    }
    cout << endl;
  }
  cout << endl;
}


//prints out found solution, if the solution does not exists such as it equals 0, it prints out the possible solutions
void Sudoku::PrintSolution(Coordinate coord){
  if (board[coord.row][coord.column].possibleSolutions[0] == 0){
    for (int i = 1; i < 10; i++){
      if (board[coord.row][coord.column].possibleSolutions[i] == 1){
        cout << i << " ";
      }
    }
    cout << endl;
  }else{
    cout << board[coord.row][coord.column].possibleSolutions[0] << endl;
  }
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//determines how many possible solutioins there are left
int Sudoku::checkHowManyPossibleSolutions(Coordinate coord){
  int counter = 0;
  for (int i = 1; i < 10; i++){
    if (board[coord.row][coord.column].possibleSolutions[i] == 1){
      counter++;
    }
  }
  return counter;
}

//for determining the only solution left
int Sudoku::getPossibleSolutioin(Coordinate coord){
  for (int i = 1; i < 10; i++){
    if (board[coord.row][coord.column].possibleSolutions[i] == 1){
      return i;
    }
  }
  return 0;
}


//determines if the bored is solved yet
bool Sudoku::isSolved(void){
  int counter = 0;
  for (int c = 0; c < 9; c++) {
    for (int i = 0; i < 9; i++) {
      counter = board[i][c].possibleSolutions[0] == 0 ? counter : counter + 1;
    }
  }

  return counter == 81 ? true : false;
}


// returns true if the given cell is empty, i.e., has no solution yet,
// false otherwise
// the row,column coordinates specify the given cell
bool Sudoku::IsEmpty(Coordinate coord){
  bool isEmpty = board[coord.row][coord.column].possibleSolutions[0] == 0 ? true : false;
  return isEmpty;
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//for threading to ensure no race conditions
//pthread_mutex_t mutex;

//args for void* args (I thought i would do this program multi threaded,
//but decied against it but did not want to revert void* functions as that would waste to much time
struct ThreadArgs{
  bool status;
  int numToSearch;
};


///////////////////////////////////////////////////////////////////////////////////////////////////////
//GET Cell from rows, columns, quadrants

// initializes CellsInRow by filling in the coordinates
// for the cells in the row containing the given cell
// the row,column coordinates specify the given cell
//for threading using void ptr
void* Sudoku::GetRowCells(void* vcoord){
  Coordinate* coord = static_cast<Coordinate*>(vcoord);
  int j = 0;
  for (int i = 0; i < 9; i++){
    if (i != coord->row){
      CellsInRow[j].row = i;
      CellsInRow[j].column = coord->column;
      j++;
    }
  }
  return nullptr;
}

// initializes CellsInColumn by filling in the coordinates
// for the cells in the column containing the given cell
// the row,column coordinates specify the given cell
//for threading using void ptr
void* Sudoku::GetColumnCells(void* vcoord){
  Coordinate* coord = static_cast<Coordinate*>(vcoord);
  int j = 0;
  for (int i = 0; i < 9; i++){
    if (i != coord->column){
      CellsInColumn[j].column= i;
      CellsInColumn[j].row = coord->row;
      j++;
    }
  }
  return nullptr;
}


// initializes CellsInQuadrant by filling in the coordinates
// for the cells in the quadrant containing the given cell
// the row,column coordinates specify the given cell

//void Sudoku::GetQuadrantCells(Coordinate coord){
//for threading using void ptr
void* Sudoku::GetQuadrantCells(void* vcoord){
  //derefrencing and casting to coord
  Coordinate* coord = static_cast<Coordinate*>(vcoord);
  int index = 0;
  //if coord->row which is 0 is less than coord->row + 3 which would be 3 increment + 1
  //so if coord->row was 3 it would go through 3, 4, 5
  //for (int i = ((coord->row / 3) * 3); i < ((coord->row / 3) * 3) + 3; ++i) {
  //same thing but with %
  for (int i = (coord->row - (coord->row % 3)); i < (coord->row - (coord->row % 3)) + 3; i++) {
    //same as previous for loop but for column
    //for (int j = ((coord->column / 3) * 3); j < ((coord->column / 3) * 3) + 3; ++j) {
    for (int j = (coord->column - (coord->column % 3)); j < (coord->column - (coord->column % 3)) + 3; j++) {
      if (i != coord->row || j != coord->column) {
        CellsInQuadrant[index].row = i;
        CellsInQuadrant[index].column = j;
        index++;
      }
    }
  }
  return nullptr;
}



///////////////////////////////////////////////////////////////////////////////////////////////////////
//search quadrants cells, row cells, column cells

//pthread_mutex_t Sudoku::mutex = PTHREAD_MUTEX_INITIALIZER;

//helper funcs to SearchInSolution as to not have it to complex of a func
void* Sudoku::SearchQuadrantCells(void* vargs){
  ThreadArgs* args = static_cast<ThreadArgs*>(vargs);
  for (int i = 0; i < 8; i++){
 //   pthread_mutex_lock(&mutex);
    //checking if any Quadrant cells have the same num as numToSearch if so return true, have to increment by 1 as index 0 is the final solution
    args->status = board[CellsInQuadrant[i].row][CellsInQuadrant[i].column].possibleSolutions[0] == args->numToSearch ? true : false;
  //  pthread_mutex_unlock(&mutex);
    if (args->status == true){
      return args;
    }
  }
  args->status = false;
  return args;
}

void* Sudoku::SearchColumnCells(void* vargs){
  ThreadArgs* args = static_cast<ThreadArgs*>(vargs);
  for (int i = 0; i < 8; i++){
//    pthread_mutex_lock(&mutex);
    //checking if any Quadrant cells have the same num as numToSearch if so return true, have to increment by 1 as index 0 is the final solution
    args->status = board[CellsInColumn[i].row][CellsInColumn[i].column].possibleSolutions[0] == args->numToSearch ? true : false;
//    pthread_mutex_unlock(&mutex);
    if (args->status == true){
      return args;
    }
  }
  args->status = false;
  return args;
}

//searches row cells for possible solutions
void* Sudoku::SearchRowCells(void* vargs){
  ThreadArgs* args = static_cast<ThreadArgs*>(vargs);
  for (int i = 0; i < 8; i++){
//    pthread_mutex_lock(&mutex);
    //checking if any Quadrant cells have the same num as numToSearch if so return true, have to increment by 1 as index 0 is the final solution
    args->status = board[CellsInRow[i].row][CellsInRow[i].column].possibleSolutions[0] == args->numToSearch ? true : false;
//    pthread_mutex_unlock(&mutex);
    if (args->status == true){
      return args;
    }
  }
  args->status = false;
  return args;
}

// This is a helper function for findPossibleSolutions
// For all the cells in the CellsInRow, CellsInColumn and CellsInQuadrant arrays
// that already have a solution
// search the solution in those cells for the given number
// (i.e. see if the solution in those cells is equal to the given number)
// returns true if the given number is found, false otherwise
bool Sudoku::SearchInSolution(int number){
  //made struct for args for void* funcs for threading
  ThreadArgs args;
  args.status = false;
  args.numToSearch = number;
  //gave up on multi threading as I didn't realize that the functions had to be either non member or static member funcs
  /*
  pthread_t quadrant_thread;
  pthread_t row_thread;
  pthread_t column_thread;

  pthread_create(&quadrant_thread, nullptr, SearchQuadrantCells, static_cast<void*>(&args));
  pthread_create(&row_thread, nullptr, SearchRowCells, static_cast<void*>(&args));
  pthread_create(&column_thread, nullptr, SearchColumnCells, static_cast<void*>(&args));

  pthread_join(quadrant_thread, nullptr);
  pthread_join(row_thread, nullptr);
  pthread_join(column_thread, nullptr);
  */

  //casting args to void* to be able to be passed into funcs as i was intending to do multi threading
  SearchQuadrantCells(static_cast<void*>(&args));
  if (args.status == true){
    return args.status;
  }
  SearchRowCells(static_cast<void*>(&args));
  if (args.status == true){
    return args.status;
  }
  SearchColumnCells(static_cast<void*>(&args));
  return args.status;
}




/////////////////////////////////////////////////////////////////////////////////////////////////////////
//searching functions for possible solutions

//helper funcs to SearchInPossible as to not have it to complex of a func
//these funcs increment everytime they see that a possible solution is not possible in cell
//then returns true only if it incremented 8 times
void* Sudoku::SearchPossibleColumnCells(void* vargs){
  ThreadArgs* args = static_cast<ThreadArgs*>(vargs);
  for (int i = 0; i < 8; i++){
    if(board[CellsInColumn[i].row][CellsInColumn[i].column].possibleSolutions[args->numToSearch] == 1 && IsEmpty(CellsInColumn[i]) == true){
      args->status = true;
      return args;
    }
  }
  args->status = false;
  return args;
}

//searches row cells for possible solutions
void* Sudoku::SearchPossibleRowCells(void* vargs){
  ThreadArgs* args = static_cast<ThreadArgs*>(vargs);
  for (int i = 0; i < 8; i++){
    if (board[CellsInRow[i].row][CellsInRow[i].column].possibleSolutions[args->numToSearch] == 1 && IsEmpty(CellsInRow[i]) == true){
      args->status = true;
      return args;
    }
  }
  args->status = false;
  return args;
}

void* Sudoku::SearchPossibleQuadrantCells(void* vargs){
  ThreadArgs* args = static_cast<ThreadArgs*>(vargs);
  for (int i = 0; i < 8; i++){
    if (board[CellsInQuadrant[i].row][CellsInQuadrant[i].column].possibleSolutions[args->numToSearch] == 1 && IsEmpty(CellsInQuadrant[i]) == true){
      args->status = true;
      return args;
    }
  }
  args->status = false;
  return args;
}

// This is a helper function for solvePuzzle
// For all the cells in the CellsInRow, CellsInColumn and CellsInQuadrant arrays
// that already have a solution
// search the possible solution in those cells for the given number
// (i.e. see if the solution in those cells is equal to the given number)
// returns true if the given number is found, false otherwise
bool Sudoku::SearchPossibleInSolution(int number){
  //made struct for args for void* funcs for threading
  ThreadArgs args;
  args.status = false;
  args.numToSearch = number;
  bool columnCells = false;
  bool rowCells = false;


  SearchPossibleColumnCells(static_cast<void*>(&args));

  if (args.status == true){
    columnCells = true;
  }

  SearchPossibleRowCells(static_cast<void*>(&args));

  if (args.status == true){
    rowCells = true;
  }

  if (rowCells == true && columnCells == true){
//    return true;
  }

  SearchPossibleQuadrantCells(static_cast<void*>(&args));


  return args.status;
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//finds impossible solutions and assigns them the value of 0
void Sudoku::findPossibleSolutions(Coordinate coord){
  if (board[coord.row][coord.column].possibleSolutions[0] != 0){
    return;
  }

  std::vector<int> numsFound;

  //this would be step 1 in dr hwang's template
  //searchInsolutioin searches what nums are already in place in the same column, row, quadrant
  for (int i = 1; i < 10; i++){
    if (SearchInSolution(i)){
      numsFound.push_back(i);
    }
  }

  //sorts and gets rid of duplicate nums
  std::sort(numsFound.begin(), numsFound.end());
  auto last = std::unique(numsFound.begin(), numsFound.end(), [](const int& a, const int& b) {
      return a == b;
    }
  );

  numsFound.erase(last, numsFound.end());

  //if nums are found in same quadrant, row, or column,
  //they are turned to 0 to indicate that they are not possible solutions
  for (int i : numsFound){
    board[coord.row][coord.column].possibleSolutions[i] = 0;
  }
}


//sovles the board
void Sudoku::SolvePuzzle(){
  //checking for singular possible answers
  //running 3 times just to make sure everything is found
  for (int l = 0; l < 3; l++){
    for (int i = 0; i < 9; i++){
      for (int j = 0; j < 9; j++){
        Coordinate cell;
        cell.row = i;
        cell.column = j;
        if (!IsEmpty(cell)){
          continue;
        }
        //casting cell to void* for it to be passed to the funcs
        GetRowCells(static_cast<void*>(&cell));
        GetColumnCells(static_cast<void*>(&cell));
        GetQuadrantCells(static_cast<void*>(&cell));

        findPossibleSolutions(cell);

        //this would be step 2 in dr hwangs template
        if (checkHowManyPossibleSolutions(cell) == 1){
          board[cell.row][cell.column].possibleSolutions[0] = getPossibleSolutioin(cell);
          continue;
        }

        for (int k = 1; k < 10; k++){
          if (SearchPossibleInSolution(k) == false && SearchInSolution(k) == false && board[cell.row][cell.column].possibleSolutions[k] == 1){
            board[cell.row][cell.column].possibleSolutions[0] = k;
          }
        }

      }
    }
  }
}



/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//for testing the puzzle, not sure how I will use this but dr hwang had it so i will to
void Sudoku::testing() {
  createPuzzle();
  for (int i = 0; i < 9; i++){
    for (int j = 0; j < 9; j++){
      Coordinate boardCells;
      boardCells.row = i;
      boardCells.column = j;
      GetRowCells(static_cast<void*>(&boardCells));
      GetColumnCells(static_cast<void*>(&boardCells));
      GetQuadrantCells(static_cast<void*>(&boardCells));
      findPossibleSolutions(boardCells);
    }
  }
  cout << "Initial puzzle" << endl;
  printBoard();
  Coordinate cell;
  cell.row = 6;
  cell.column = 2;
  //casting cell to void* for it to be passed to the funcs
  GetRowCells(static_cast<void*>(&cell));
  GetColumnCells(static_cast<void*>(&cell));
  GetQuadrantCells(static_cast<void*>(&cell));

  /*
  cout << (IsEmpty(cell) ? "true" : "false") << endl;

  for (int i = 0; i < 8; i++){
    cout << CellsInRow[i].row << " ";
  }
  cout << endl;
  for (int i = 0; i < 8; i++){
    cout << CellsInRow[i].column << " ";
  }
  cout << endl;
  cout << endl;
  for (int i = 0; i < 8; i++){
    cout << CellsInColumn[i].row << " ";
  }
  cout << endl;
  for (int i = 0; i < 8; i++){
    cout << CellsInColumn[i].column << " ";
  }
  cout << endl;
  cout << endl;

  for (int i = 0; i < 8; i++){
    cout << CellsInQuadrant[i].row << " ";
  }
  cout << endl;
  for (int i = 0; i < 8; i++){
    cout << CellsInQuadrant[i].column << " ";
  }
  cout << endl;
  cout << endl;
  */

  cout << (SearchInSolution(9) ? "true" : "false") << endl;
  cout << (SearchPossibleInSolution(9) ? "true" : "false") << endl;
  //cout << (SearchAdjacent(4) ? "true" : "false") << endl;

  /*
  ThreadArgs args;
  args.status = false;
  args.numToSearch = 5;
  SearchAdjacentColumnCells(static_cast<void*>(&args));
  cout << (args.status ? "true" : "false") << endl;
  SearchAdjacentRowCells(static_cast<void*>(&args));
  cout << (args.status ? "true" : "false") << endl;


  cout << endl;

  findPossibleSolutions(cell);
 // PrintSolution(cell);
 */
}




/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/* UNUSED CODE, DO NOT USE, JUST WANT TO KEEP FOR FUTURE REFRENCE OF WHAT I DID WRONG AND HOW TO IMPROVE UPON IT */



/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//searches cells in columns and rows next to currenct cell
//do not need (probably)

/*
void* Sudoku::SearchAdjacentColumnCells(void* vargs){
  ThreadArgs* args = static_cast<ThreadArgs*>(vargs);
  int checkAll = 0;
  int otherColumns[2] = { 0, 0 };
  int otherRows[2] = { 0, 0 };
  for (int i = 0; i < 2; i++){
    for (int j = 0; j < 8; j++){
      if (otherRows[0] != CellsInQuadrant[j].row && CellsInQuadrant[j].row != CellsInColumn[0].row){
        otherRows[i] = CellsInQuadrant[j].row;
      }
    }
  }

  for (int i = 0; i < 2; i++){
    for (int j = 0; j < 8; j++){
      if (otherColumns[0] != CellsInQuadrant[j].column && CellsInQuadrant[j].column != CellsInRow[0].column){
        otherColumns[i] = CellsInQuadrant[j].column;
      }
    }
  }

  for (int i = 0; i < 2; i++){
    if (board[CellsInColumn[0].row][otherColumns[i]].possibleSolutions[0] == 0){
      checkAll++;
    }
  }
  if (checkAll == 2){
    args->status = false;
    return args;
  }
  checkAll = 0;
  for (int i = 0; i < 2; i++){
    for (int j = 0; j < 9; j++){
      checkAll = board[j][otherColumns[i]].possibleSolutions[0] == args->numToSearch ? checkAll + 1 : checkAll;
      //cout << board[j][otherColumns[i]].possibleSolutions[0] << " at " << j << otherColumns[i] << endl;
    }
  }
  args->status = checkAll >= 2 ? true : false;
  return args;
}

void* Sudoku::SearchAdjacentRowCells(void* vargs){
  ThreadArgs* args = static_cast<ThreadArgs*>(vargs);
  int checkAll = 0;
  int otherColumns[2] = { 0, 0 };
  int otherRows[2] = { 0, 0 };
  for (int i = 0; i < 2; i++){
    for (int j = 0; j < 8; j++){
      if (otherRows[0] != CellsInQuadrant[j].row && CellsInQuadrant[j].row != CellsInColumn[0].row){
        otherRows[i] = CellsInQuadrant[j].row;
      }
    }
  }

  for (int i = 0; i < 2; i++){
    for (int j = 0; j < 8; j++){
      if (otherColumns[0] != CellsInQuadrant[j].column && CellsInQuadrant[j].column != CellsInRow[0].column){
        otherColumns[i] = CellsInQuadrant[j].column;
      }
    }
  }

  for (int i = 0; i < 2; i++){
    if (board[CellsInColumn[0].row][otherColumns[i]].possibleSolutions[0] == 0){
      checkAll++;
    }
  }

  if (checkAll == 2){
    args->status = false;
    return args;
  }
  checkAll = 0;
  for (int i = 0; i < 2; i++){
    for (int j = 0; j < 9; j++){
      if (CellsInQuadrant[i].row != CellsInColumn[0].row){
        checkAll = board[otherRows[i]][j].possibleSolutions[0] == args->numToSearch ? checkAll + 1 : checkAll;
      }
    }
  }
  args->status = checkAll >= 2 ? true : false;
  return args;
}

bool Sudoku::SearchAdjacent(int number){
  ThreadArgs args;
  args.status = false;
  args.numToSearch = number;

  //casting args to void* to be able to be passed into funcs as i was intending to do multi threading
  SearchAdjacentRowCells(static_cast<void*>(&args));
  if (args.status)
    return args.status;

  SearchAdjacentColumnCells(static_cast<void*>(&args));
  return args.status;
}



*/




////////////////////////////////////////////////

  //this does not work, but I will keep it for future refrence
  //was trying to replikcate how the website sudoku9x9.com solver works, but could not fully understand it
/*
  //checking for non naked solutioins
  for (int l = 0; l < 3; l++){
    for (int i = 0; i < 9; i++){
      for (int j = 0; j < 9; j++){
        Coordinate cell;
        cell.row = i;
        cell.column = j;
        if (!IsEmpty(cell)){
          continue;
        }
        //casting cell to void* for it to be passed to the funcs
        GetRowCells(static_cast<void*>(&cell));
        GetColumnCells(static_cast<void*>(&cell));
        GetQuadrantCells(static_cast<void*>(&cell));

        findPossibleSolutions(cell);

        for (int l = 1; l < 10; l++){
          if (SearchPossibleInSolution(l) == true && board[cell.row][cell.column].possibleSolutions[l] == 1){
            board[cell.row][cell.column].possibleSolutions[0] = l;
            continue;
          }
        }

        for (int l = 1; l < 10; l++){
          if (SearchAdjacent(l) == true && board[cell.row][cell.column].possibleSolutions[l] == 1){
            board[cell.row][cell.column].possibleSolutions[0] = l;
            continue;
          }
        }

      }
    }
  }

  */


////////////////////////////////////////////////


// Constructor
// initialize all to 0, which I do not want / need
/*
Sudoku::Sudoku() {
  for (int r = 0; r < 9; r++) {
    for (int c = 0; c < 9; c++) {
      for (int i = 0; i < 10; i++) {
        board[r][c].possibleSolutions[i] = 0;
      }
    }
  }
}
*/
